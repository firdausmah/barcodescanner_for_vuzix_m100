package com.example.barcodescanner.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.PlanarYUVLuminanceSource;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;

import java.io.IOException;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class MainActivity extends Activity implements SurfaceHolder.Callback, ZXingScannerView.ResultHandler {
    private static final String TAG = MainActivity.class.getSimpleName();


    private static final int REQUEST_CAMERA = 1;
    private static final int permissions = 1;
    private ZXingScannerView scannerView;

    private String mAmazonUrl;

    private Camera mCamera;

    private SurfaceView mSurfaceView;

    private final MultiFormatReader mMultiFormatReader = new MultiFormatReader();

    private final Camera.AutoFocusCallback mAutoFocusCallback = new Camera.AutoFocusCallback() {

        @Override
        public void onAutoFocus(boolean success, Camera camera) {
            if (success) {
                camera.setOneShotPreviewCallback(mPreviewCallback);
            } else {
                Toast.makeText(getApplicationContext(), "focus failed.", Toast.LENGTH_SHORT).show();
            }

            if (mCamera != null) {
                mCamera.autoFocus(this);
            }
        }
    };

    private final Camera.PreviewCallback mPreviewCallback = new Camera.PreviewCallback() {
        @Override
        public void onPreviewFrame(byte[] data, Camera camera) {
            Camera.Size size = camera.getParameters().getPreviewSize();
            PlanarYUVLuminanceSource source = new PlanarYUVLuminanceSource(data, size.width,
                    size.height, 0, 0, size.width, size.height, false);

            BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
            try {
                Result rawResult = mMultiFormatReader.decode(bitmap);

                if (rawResult.getBarcodeFormat() == BarcodeFormat.EAN_13) {
                    search(rawResult.getText());
                }
            } catch (NotFoundException e) {
                Log.e(TAG, "IOException", e);

            }
        }
    };

    private void search(String ean13) {
        String url = mAmazonUrl + ean13;
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        scannerView = new ZXingScannerView(this);
        setContentView(scannerView);
    }

    @Override
    protected void onResume() {
        super.onResume();


        if(scannerView == null) {
            scannerView = new ZXingScannerView(MainActivity.this);
            setContentView(scannerView);
        }
        scannerView.startCamera();
        scannerView.setResultHandler(this);

    }


    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        mCamera = Camera.open();

        try {
            mCamera.setPreviewDisplay(surfaceHolder);
        } catch (IOException e) {
            Log.e(TAG, "IOException", e);
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int format, int width, int height) {
        mCamera.startPreview();
        mCamera.autoFocus(mAutoFocusCallback);
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
//        if (mCamera != null) {
//            mCamera.cancelAutoFocus();
//            mCamera.release();
//            mCamera = null;
//        }

        super.onDestroy();
        scannerView.stopCamera();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_MENU) {
            LicenseDialog.create(this).show();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void handleResult(Result result) {
        final String myResult = result.getText();
        Log.d("QRCodeScanner", result.getText());
        Log.d("QRCodeScanner", result.getBarcodeFormat().toString());

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Scan Result");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                scannerView.resumeCameraPreview(MainActivity.this);
            }
        });
        builder.setNeutralButton("Visit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(myResult));
                startActivity(browserIntent);
            }
        });
        builder.setMessage(result.getText());
        AlertDialog alert1 = builder.create();
        alert1.show();
    }

}
